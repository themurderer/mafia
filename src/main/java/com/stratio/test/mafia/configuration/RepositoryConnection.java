package com.stratio.test.mafia.configuration;

import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.domain.Relationship;
import org.jgrapht.graph.DefaultDirectedGraph;

public class RepositoryConnection {

  private static DefaultDirectedGraph<Member, Relationship> graph = null;

  public static DefaultDirectedGraph<Member, Relationship> getConnection() {
    if (graph == null) {
      graph = new DefaultDirectedGraph<>(Relationship.class);
    }
    return graph;
  }
}
