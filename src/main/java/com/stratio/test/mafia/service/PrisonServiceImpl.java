package com.stratio.test.mafia.service;

import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.domain.Relationship;
import com.stratio.test.mafia.exception.NotDefinedYet;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.PrisonService;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class PrisonServiceImpl implements PrisonService {

  private final MemberRepository memberRepository;

  private final HashMap<Integer, Member> jail;

  public PrisonServiceImpl(MemberRepository memberRepository, HashMap<Integer, Member> jail) {
    this.memberRepository = memberRepository;
    this.jail = jail;
  }

  private List<Member> mapRelationShipMemberToSubordinateList(Set<Relationship> relationshipMembers) {
    return relationshipMembers.stream().map(Relationship::getSubordinate).collect(Collectors.toList());
  }

  private List<Member> mapRelationShipMemberToBossList(Set<Relationship> relationshipMembers) {
    return relationshipMembers.stream().map(Relationship::getBoss).collect(Collectors.toList());
  }

  private Integer compareMemberBirthDateASC(Member m1, Member m2) {
    return m1.getBirthDate().compareTo(m2.getBirthDate());
  }

  private void assignNewBossToSubordinates(Member memberGoesToJail, List<Member> subordinates, Member boss) {
    Member newBoss;

    if (!subordinates.isEmpty()) {
      List<Member> membersSameDepth = memberRepository.getAllMemberWithSameDepth(memberGoesToJail);

      // Choosing new boss
      if (!membersSameDepth.isEmpty()) {
        newBoss = membersSameDepth.stream().min(this::compareMemberBirthDateASC).get();
      } else {
        // Don't allow self-loops (Member and boss)
        newBoss = subordinates.stream().min(this::compareMemberBirthDateASC).orElse(null);
      }

      // Modification of relationships (edge)
      List<Member> listSubordinatesModified = subordinates.stream().filter(s -> !s.equals(newBoss)).collect(Collectors.toList());

      // If is not root node
      if (boss != null) {
        memberRepository.insertRelationShip(boss, newBoss);
      }

      listSubordinatesModified.forEach(subordinate -> memberRepository.insertRelationShip(newBoss, subordinate));
    }
  }

  private void removeBossOfLastSubordinates(Member memberReleased) {
    if (memberReleased.getLastSubordinates() != null) {
      memberReleased.getLastSubordinates().forEach(sub -> {
        Member subInGraph = memberRepository.getMemberByIdentifier(sub.getIdentifier());

        if (subInGraph != null) {
          memberRepository.removeIncomingEdge(subInGraph);
        }
      });
    }
  }

  private void createRelationshipWithLastSubordinates(Member memberReleased, Member memberToInsertInGraph) {
    if (memberReleased.getLastSubordinates() != null) {
      memberReleased.getLastSubordinates().forEach(sub -> {
        Member subInGraph = memberRepository.getMemberByIdentifier(sub.getIdentifier());
        if (subInGraph != null) {
          memberRepository.insertRelationShip(memberToInsertInGraph, subInGraph);
        }
      });
    }
  }

  private void createRelationShipWithLastBoss(Member memberReleased, Member memberToInsertInGraph) {
    if (memberReleased.getLastBoss() != null) {
      Member lastBoss = memberRepository.getMemberByIdentifier(memberReleased.getLastBoss().getIdentifier());
      memberRepository.insertRelationShip(lastBoss, memberToInsertInGraph);
    }
  }

  private Member getBoss(Member memberGoesToJail) {
    Member boss = null;
    Set<Relationship> relationshipMemberBoss = memberRepository.getParentMember(memberGoesToJail);
    List<Member> bossList = this.mapRelationShipMemberToBossList(relationshipMemberBoss);

    // Only can have one boss unique
    if (!bossList.isEmpty()) {
      boss = bossList.get(0);
    }

    return boss;
  }

  private List<Member> getSubordinates(Member memberGoesToJail){
    Set<Relationship> relationshipMembers = memberRepository.getChildMember(memberGoesToJail);
    return this.mapRelationShipMemberToSubordinateList(relationshipMembers);
  }

  public void goToJail(Integer identifier) {

    Member memberGoesToJail = memberRepository.getMemberByIdentifier(identifier);
    if (memberGoesToJail != null) {

      // Getting basic information
      List<Member> subordinates = getSubordinates(memberGoesToJail);
      Member boss = getBoss(memberGoesToJail);

      assignNewBossToSubordinates(memberGoesToJail, subordinates, boss);

      // Remove Mafia an goes to jail
      memberRepository.removeMember(memberRepository.getMemberByIdentifier(identifier));

      memberGoesToJail.setLastBoss(boss);
      memberGoesToJail.setLastSubordinates(subordinates);
      jail.put(memberGoesToJail.getIdentifier(), memberGoesToJail);
    }
  }

  public void releasedFromPrison(Integer identifier) {
    Member memberReleased = jail.remove(identifier);
    if (!memberRepository.existsMember(identifier) && memberReleased != null) {
      if (memberReleased.getLastBoss() != null && !memberRepository.existsMember(memberReleased.getLastBoss().getIdentifier())) {
        throw new NotDefinedYet();
      }

      // Removing incoming edge the last subordinates
      removeBossOfLastSubordinates(memberReleased);

      // Inserting new member
      Member memberToInsertInGraph = new Member.Builder().withIdentifier(memberReleased.getIdentifier()).withName(memberReleased.getName())
          .withBirthDate(memberReleased.getBirthDate()).build();
      memberRepository.insertMember(memberToInsertInGraph);

      createRelationShipWithLastBoss(memberReleased, memberToInsertInGraph);
      createRelationshipWithLastSubordinates(memberReleased, memberToInsertInGraph);
    } else {
      throw new OperationNotAllowed("Member must be in prison to be released");
    }
  }

}
