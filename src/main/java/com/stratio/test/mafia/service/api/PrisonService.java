package com.stratio.test.mafia.service.api;

import com.stratio.test.mafia.exception.OperationNotAllowed;

public interface PrisonService {

  void goToJail(Integer identifier);

  void releasedFromPrison(Integer identifier);

}
