package com.stratio.test.mafia.service.api;

import com.stratio.test.mafia.exception.OperationNotAllowed;
import java.time.LocalDateTime;

public interface HireService {

  void hireNewMember(Integer numberIdentification, String name, LocalDateTime birthDate, Integer bossId);
}
