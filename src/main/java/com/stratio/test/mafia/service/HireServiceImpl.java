package com.stratio.test.mafia.service;

import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.HireService;
import java.time.LocalDateTime;
import org.springframework.stereotype.Service;

@Service
public class HireServiceImpl implements HireService {

  private final MemberRepository memberRepository;

  public HireServiceImpl(MemberRepository memberRepository) {
    this.memberRepository = memberRepository;
  }

  private void createMemberWithBoss(Member newMember, Integer bossId) throws OperationNotAllowed {
    Member boss = memberRepository.getMemberByIdentifier(bossId);
    if (boss == null) {
      throw new MemberNotFound();
    }
    memberRepository.insertMember(newMember);
    memberRepository.insertRelationShip(boss, newMember);
  }

  public void hireNewMember(Integer numberIdentification, String name, LocalDateTime birthDate, Integer bossId){

    Member newMember = new Member.Builder().withIdentifier(numberIdentification).withName(name).withBirthDate(birthDate).build();
    if (bossId == null) {
      memberRepository.insertMemberInLeafNode(newMember);
    } else {
      createMemberWithBoss(newMember, bossId);
    }
  }
}
