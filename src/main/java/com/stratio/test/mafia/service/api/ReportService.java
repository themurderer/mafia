package com.stratio.test.mafia.service.api;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import java.util.List;

public interface ReportService {

  List<MemberResponse> specialSurveillance();

}
