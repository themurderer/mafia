package com.stratio.test.mafia.service;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.converter.MemberToMemberResponseConverter;
import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.MemberService;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl implements MemberService {

  private final MemberRepository memberRepository;

  private final MemberToMemberResponseConverter converter;

  public MemberServiceImpl(MemberRepository memberRepository, MemberToMemberResponseConverter converter) {
    this.memberRepository = memberRepository;
    this.converter = converter;
  }

  private Member obtainMember(Integer identifier) {
    Member member = memberRepository.getMemberByIdentifier(identifier);
    if (member == null) {
      throw new MemberNotFound();
    }

    return member;
  }

  @Override
  public MemberResponse getMember(Integer identifier) {
    return converter.convert(obtainMember(identifier));

  }

}
