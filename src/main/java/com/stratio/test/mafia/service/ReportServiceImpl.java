package com.stratio.test.mafia.service;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.converter.MemberToMemberResponseConverter;
import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.ReportService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {

  private Integer limitChildToSpecialSurveillance;

  private final MemberRepository memberRepository;
  private final MemberToMemberResponseConverter converter;

  private Boolean haveSpecialSurveillance(Member memberTarget) {

    Integer numberOfChild = memberRepository.getNumberOfChildMember(memberTarget);
    return numberOfChild != null && numberOfChild > limitChildToSpecialSurveillance;

  }

  public ReportServiceImpl(@Value("${mafia.limit.surveillance:50}") Integer limitChildToSpecialSurveillance, MemberRepository memberRepository,
      MemberToMemberResponseConverter converter) {
    this.limitChildToSpecialSurveillance = limitChildToSpecialSurveillance;
    this.memberRepository = memberRepository;
    this.converter = converter;
  }

  public List<MemberResponse> specialSurveillance() {
    List<Member> allMembers = memberRepository.getAllMember();
    List<Member> specialMembers = allMembers.stream().filter(this::haveSpecialSurveillance).collect(Collectors.toList());

    return specialMembers.stream().map(converter::convert).collect(Collectors.toList());
  }
}
