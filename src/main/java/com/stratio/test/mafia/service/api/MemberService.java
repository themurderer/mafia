package com.stratio.test.mafia.service.api;

import com.stratio.test.mafia.controller.vo.MemberResponse;

public interface MemberService {

  MemberResponse getMember(Integer identifier);

}
