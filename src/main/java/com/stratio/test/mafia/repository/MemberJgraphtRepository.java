package com.stratio.test.mafia.repository;

import com.stratio.test.mafia.configuration.RepositoryConnection;
import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.domain.Relationship;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.jgrapht.Graph;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.springframework.stereotype.Repository;

@Repository
public class MemberJgraphtRepository implements MemberRepository {

  private final Graph<Member, Relationship> graph;

  public MemberJgraphtRepository() {
    this.graph = RepositoryConnection.getConnection();
  }

  private Integer getDepth(Member target) {
    BreadthFirstIterator<Member, Relationship> iterator = new BreadthFirstIterator<>(graph);
    while (iterator.hasNext()) {
      if (target.equals(iterator.next())) {
        return iterator.getDepth(target);
      }
    }
    throw new MemberNotFound();
  }

  public Member getMemberByName(String name) {
    if (name != null) {
      return graph.vertexSet().stream().filter(p -> name.equals(p.getName())).findFirst().orElse(null);
    }
    return null;

  }

  public Member getMemberByIdentifier(Integer identifier) {
    if (identifier != null) {
      return graph.vertexSet().stream().filter(p -> identifier.equals(p.getIdentifier())).findFirst().orElse(null);
    }
    return null;
  }

  public Set<Relationship> getChildMember(Member target) {
    if (target.getIdentifier() != null) {
      Member member = this.getMemberByIdentifier(target.getIdentifier());
      if (member != null) {
        return graph.outgoingEdgesOf(member);
      }
    }
    throw new MemberNotFound();
  }

  public Set<Relationship> getParentMember(Member target) {
    if (target.getIdentifier() != null) {
      Member member = this.getMemberByIdentifier(target.getIdentifier());
      if (member != null) {
        return graph.incomingEdgesOf(member);
      }
    }
    throw new MemberNotFound();
  }

  public List<Member> getAllMemberWithSameDepth(Member target) {
    List<Member> membersSameDepth = new ArrayList<>();
    Integer depth = this.getDepth(target);
    if (target != null) {
      BreadthFirstIterator<Member, Relationship> iterator = new BreadthFirstIterator<>(graph);
      while (iterator.hasNext()) {
        Member itMember = iterator.next();
        if (depth.equals(iterator.getDepth(itMember)) && !target.equals(itMember)) {
          membersSameDepth.add(itMember);
        }
      }
    }

    return membersSameDepth;
  }

  public List<Member> getAllMember() {
    List<Member> members = new ArrayList();
    BreadthFirstIterator<Member, Relationship> iterator = new BreadthFirstIterator<>(graph);
    while (iterator.hasNext()) {
      members.add(iterator.next());
    }
    return members;
  }

  public Integer getNumberOfChildMember(Member target) {
    if (target.getIdentifier() != null) {
      Member member = this.getMemberByIdentifier(target.getIdentifier());
      if (member != null) {
        return graph.outDegreeOf(member);
      }
    }
    throw new MemberNotFound();
  }

  public Boolean existsMember(Integer identifier) {
    return getMemberByIdentifier(identifier) != null;
  }

  public void insertMember(Member target) {
    if (!existsMember(target.getIdentifier())) {
      graph.addVertex(target);
    } else {
      throw new OperationNotAllowed("Member exists in new system");
    }
  }

  public void insertRelationShip(Member source, Member target) {
    if (existsMember(target.getIdentifier()) && existsMember(source.getIdentifier())) {
      graph.addEdge(source, target);
    }else{
      throw new OperationNotAllowed("Member must exists");
    }
  }

  public void insertMemberInLeafNode(Member memberTarget) {

    // The target member is inserted in oldest leaf node member
    Optional<Member> optionalMember = graph.vertexSet().stream()
        .filter(key -> graph.outgoingEdgesOf(key).size() == 0)
        .min(Comparator.comparing(Member::getBirthDate));

    this.insertMember(memberTarget);
    optionalMember.ifPresent(memberSource -> insertRelationShip(memberSource, memberTarget));
  }

  public Boolean removeMember(Member target) {
    return graph.removeVertex(target);
  }

  public Boolean removeIncomingEdge(Member target) {
    if(this.existsMember(target.getIdentifier())) {
      return graph.removeAllEdges(graph.incomingEdgesOf(target));
    }else{
      throw new OperationNotAllowed("Member must exists");
    }
  }

  public Relationship removeEdge(Member source, Member target) {
    return graph.removeEdge(source, target);
  }
}
