package com.stratio.test.mafia.repository;

import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.domain.Relationship;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import java.util.List;
import java.util.Set;

public interface MemberRepository {

  Member getMemberByName(String name);

  Member getMemberByIdentifier(Integer identifier);

  Boolean existsMember(Integer identifier);

  // Functions to obtain parent, child and same depth members given a specific existing member

  Set<Relationship> getParentMember(Member target);

  List<Member> getAllMemberWithSameDepth(Member target);

  Set<Relationship> getChildMember(Member target); // review

  Integer getNumberOfChildMember(Member target);

  List<Member> getAllMember();

  // Insertions of member

  void insertMember(Member target);

  void insertMemberInLeafNode(Member memberTarget);

  void insertRelationShip(Member source, Member target);

  // Remove member

  Boolean removeMember(Member target);

  Boolean removeIncomingEdge(Member target);

  Relationship removeEdge(Member source, Member target);
}
