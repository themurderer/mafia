package com.stratio.test.mafia.controller;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.service.api.ReportService;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReportController {

  private final ReportService reportService;

  public ReportController(ReportService reportService) {
    this.reportService = reportService;
  }

  @GetMapping("/surveillance")
  public ResponseEntity<List<MemberResponse>> specialSurveillance() {
    List<MemberResponse> specialMembers = reportService.specialSurveillance();

    return new ResponseEntity<>(specialMembers, HttpStatus.OK);
  }
}
