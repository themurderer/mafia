package com.stratio.test.mafia.controller.vo;

import java.time.LocalDateTime;
import java.util.Objects;

public class MemberResponse {

  private Integer identifier;

  private String name;

  private LocalDateTime birthDate;

  public Integer getIdentifier() {
    return identifier;
  }

  public String getName() {
    return name;
  }

  public LocalDateTime getBirthDate() {
    return birthDate;
  }

  public static class Builder {

    private final MemberResponse object;

    public Builder() {
      object = new MemberResponse();
    }

    public MemberResponse.Builder withIdentifier(Integer value) {
      object.identifier = value;
      return this;
    }

    public MemberResponse.Builder withName(String value) {
      object.name = value;
      return this;
    }

    public MemberResponse.Builder withBirthDate(LocalDateTime value) {
      object.birthDate = value;
      return this;
    }

    public MemberResponse build() {
      return object;
    }

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MemberResponse that = (MemberResponse) o;
    return Objects.equals(identifier, that.identifier) && Objects.equals(name, that.name) && Objects
        .equals(birthDate, that.birthDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier, name, birthDate);
  }
}
