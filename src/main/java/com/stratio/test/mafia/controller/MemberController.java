package com.stratio.test.mafia.controller;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.service.api.MemberService;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {

  private final MemberService memberService;

  public MemberController(MemberService memberService) {
    this.memberService = memberService;
  }

  @GetMapping("/member/{identifier}")
  public ResponseEntity<MemberResponse> obtainMember(@NotNull @PathVariable("identifier") Integer identifier) {
    MemberResponse member;
    try {
      member = memberService.getMember(identifier);
    } catch (MemberNotFound e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(member, HttpStatus.OK);
  }

}
