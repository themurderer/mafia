package com.stratio.test.mafia.controller;

import com.stratio.test.mafia.controller.vo.MemberRequest;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import com.stratio.test.mafia.service.api.HireService;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HireController {

  private final HireService hireService;

  public HireController(HireService hireService) {
    this.hireService = hireService;
  }

  @PostMapping("/member")
  public ResponseEntity<Void> insertNewMember(@Valid @RequestBody MemberRequest memberRequest){

    try {
      hireService
          .hireNewMember(memberRequest.getIdentifier(), memberRequest.getName(), memberRequest.getBirthDate(), memberRequest.getBoss());
    } catch (OperationNotAllowed e) {
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    } catch (MemberNotFound e){
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(HttpStatus.CREATED);

  }
}
