package com.stratio.test.mafia.controller;

import com.stratio.test.mafia.exception.OperationNotAllowed;
import com.stratio.test.mafia.service.api.PrisonService;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrisonController {

  private final PrisonService prisonService;

  public PrisonController(PrisonService prisonService) {
    this.prisonService = prisonService;
  }

  @PostMapping("/member/{identifier}/jailed")
  public ResponseEntity<Void> memberGoesToJail(@NotNull @PathVariable("identifier") Integer identifier) {
    prisonService.goToJail(identifier);
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }

  @PostMapping("/member/{identifier}/released")
  public ResponseEntity<Void> memberReleasedFromPrison(@NotNull @PathVariable("identifier") Integer identifier) {
    try {
      prisonService.releasedFromPrison(identifier);
    } catch (OperationNotAllowed e) {
      return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }
}
