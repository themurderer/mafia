package com.stratio.test.mafia.domain;

import org.jgrapht.graph.DefaultEdge;

public class Relationship extends DefaultEdge {

  public Relationship() {
    super();
  }

  public Member getBoss() {
    return (Member) this.getSource();
  }

  public Member getSubordinate() {
    return (Member) this.getTarget();
  }


}
