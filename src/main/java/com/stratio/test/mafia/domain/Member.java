package com.stratio.test.mafia.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Member {

  private Integer identifier;

  private String name;

  private LocalDateTime birthDate;

  private List<Member> lastSubordinates;

  private Member lastBoss;

//  private Boolean newBossSubordinate;

  public Integer getIdentifier() {
    return identifier;
  }

  public String getName() {
    return name;
  }

  public LocalDateTime getBirthDate() {
    return birthDate;
  }

  public List<Member> getLastSubordinates() {
    return this.lastSubordinates;
  }

  public void setLastSubordinates(List<Member> lastSubordinates) {
    this.lastSubordinates = lastSubordinates;
  }

  public Member getLastBoss() {
    return lastBoss;
  }

  public void setLastBoss(Member lastBoss) {
    this.lastBoss = lastBoss;
  }

  public static class Builder {

    private final Member object;

    public Builder() {
      object = new Member();
    }

    public Builder withIdentifier(Integer value) {
      object.identifier = value;
      return this;
    }

    public Builder withName(String value) {
      object.name = value;
      return this;
    }

    public Builder withBirthDate(LocalDateTime value) {
      object.birthDate = value;
      return this;
    }

    public Member build() {
      return object;
    }

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Member member = (Member) o;
    return Objects.equals(identifier, member.identifier) && Objects.equals(name, member.name)
        && Objects.equals(birthDate, member.birthDate) && Objects.equals(lastSubordinates, member.lastSubordinates)
        && Objects.equals(lastBoss, member.lastBoss);
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier, name, birthDate, lastSubordinates, lastBoss);
  }
}
