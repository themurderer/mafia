package com.stratio.test.mafia.exception;

import org.springframework.beans.factory.annotation.Value;

public class NotDefinedYet extends RuntimeException {

  private static String DEFAULT_MESSAGE = "Not implemented yet";

  public NotDefinedYet(String message, Throwable cause) {
    super(message, cause);
  }

  public NotDefinedYet() {
    super(DEFAULT_MESSAGE);
  }
}
