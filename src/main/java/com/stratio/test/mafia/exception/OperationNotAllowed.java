package com.stratio.test.mafia.exception;

public class OperationNotAllowed extends RuntimeException {

  public OperationNotAllowed(String message, Throwable cause) {
    super(message, cause);
  }

  public OperationNotAllowed(String message) {
    super(message);
  }
}
