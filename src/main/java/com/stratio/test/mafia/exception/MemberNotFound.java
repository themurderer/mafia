package com.stratio.test.mafia.exception;

public class MemberNotFound extends RuntimeException {

  private static String DEFAULT_MESSAGE = "Member Not Found";

  public MemberNotFound() {
    super(DEFAULT_MESSAGE);
  }

  public MemberNotFound(String message, Throwable cause) {
    super(message, cause);
  }
}
