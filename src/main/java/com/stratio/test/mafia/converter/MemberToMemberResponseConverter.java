package com.stratio.test.mafia.converter;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.domain.Member;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class MemberToMemberResponseConverter implements Converter<Member, MemberResponse> {

  @Override
  public MemberResponse convert(Member source) {
    if(source != null) {
      return new MemberResponse.Builder().withIdentifier(source.getIdentifier()).withName(source.getName())
          .withBirthDate(source.getBirthDate()).build();
    }
    return null;
  }
}
