package com.stratio.test.mafia.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.stratio.test.mafia.service.api.PrisonService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class PrisonControllerTest {

  private static PrisonController prisonController;

  private static PrisonService prisonService;

  @BeforeAll
  static void beforeAll() {
    prisonService = mock(PrisonService.class);
    prisonController = new PrisonController(prisonService);
  }

  @Test
  public void whenMemberGoesToJailInvoked() {
    // act
    prisonController.memberGoesToJail(1);

    //assert
    verify(prisonService).goToJail(1);
  }

  @Test
  public void whenMemberReleasedFromPrisonInvoked() {
    // act
    prisonController.memberReleasedFromPrison(1);

    //assert
    verify(prisonService).releasedFromPrison(1);
  }
}