package com.stratio.test.mafia.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.stratio.test.mafia.controller.vo.MemberRequest;
import com.stratio.test.mafia.service.api.HireService;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class HireControllerTest {

  private static HireController hireController;

  private static HireService hireService;

  @BeforeAll
  static void beforeAll() {
    hireService = mock(HireService.class);
    hireController = new HireController(hireService);
  }

  @Test
  public void whenInsertNewMemberIsInvoked() {
    // arrange
    MemberRequest memberRequest = new MemberRequest();
    memberRequest.setIdentifier(1);
    memberRequest.setName("Pepe");
    memberRequest.setBirthDate(LocalDateTime.now());

    // act
    hireController.insertNewMember(memberRequest);

    // assert
    verify(hireService).hireNewMember(1, "Pepe", memberRequest.getBirthDate(), null);
  }
}