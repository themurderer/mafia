package com.stratio.test.mafia.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.stratio.test.mafia.service.api.ReportService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ReportControllerTest {

  private static ReportController reportController;

  private static ReportService reportService;

  @BeforeAll
  static void beforeAll() {
    reportService = mock(ReportService.class);
    reportController = new ReportController(reportService);
  }

  @Test
  public void whenSpecialSurveillanceIsInvoked() {

    // act
    reportController.specialSurveillance();

    // assert
    verify(reportService).specialSurveillance();
  }
}