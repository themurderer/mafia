package com.stratio.test.mafia.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.stratio.test.mafia.service.api.MemberService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MemberControllerTest {

  private static MemberController memberController;

  private static MemberService memberService;

  @BeforeAll
  static void beforeAll() {
    memberService = mock(MemberService.class);
    memberController = new MemberController(memberService);
  }

  @Test
  public void whenObtainMemberIsInvoked() {

    // act
    memberController.obtainMember(1);

    // assert
    verify(memberService).getMember(1);
  }
}