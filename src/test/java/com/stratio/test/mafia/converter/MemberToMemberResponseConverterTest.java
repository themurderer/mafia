package com.stratio.test.mafia.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.domain.Member;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MemberToMemberResponseConverterTest {

  private static MemberToMemberResponseConverter converter;

  @BeforeAll
  static void beforeAll() {
    converter = new MemberToMemberResponseConverter();
  }

  @Test
  void givenMemberWhenConvertIsInvokedThenMemberResponseIsObtained() {
    // arrange
    Member member = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    MemberResponse memberResponse =
        new MemberResponse.Builder().withIdentifier(1).withName("Name1").withBirthDate(member.getBirthDate()).build();

    // act
    MemberResponse target = converter.convert(member);

    // assert
    assertNotNull(target);
    assertEquals(memberResponse, target);
  }

  @Test
  void givenNullWhenConvertIsInvokedThenNullIsObtained() {

    // act
    MemberResponse target = converter.convert(null);

    // assert
    assertNull(target);
  }
}