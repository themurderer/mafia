package com.stratio.test.mafia.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.domain.Relationship;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MemberJgraphtRepositoryTest {

  private static MemberRepository memberRepository;

  private static List<Member> defaultMembers = new ArrayList<>();

  @BeforeAll
  static void beforeAll() {
    memberRepository = new MemberJgraphtRepository();
    defaultMembers.add(new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build());
    defaultMembers.add(new Member.Builder().withIdentifier(2).withName("Name2").withBirthDate(LocalDateTime.now()).build());
    defaultMembers.add(new Member.Builder().withIdentifier(4).withName("Name4").withBirthDate(LocalDateTime.now()).build());
  }

  @BeforeEach
  void setUp() {
    defaultMembers.forEach(m -> memberRepository.insertMember(m));

    memberRepository.insertRelationShip(defaultMembers.get(0), defaultMembers.get(1));
    memberRepository.insertRelationShip(defaultMembers.get(0), defaultMembers.get(2));
  }

  @AfterEach
  void tearDown() {
    memberRepository.getAllMember().forEach(m -> memberRepository.removeMember(m));
  }

  @Test
  void givenNameOfMemberExistingWhenGetMemberByNameIsInvokedThenMemberIsObtained() {

    // act
    Member target = memberRepository.getMemberByName("Name1");

    // assert
    assertNotNull(target);
    assertEquals(defaultMembers.get(0), target);
  }

  @Test
  void givenNameOfMemberNotExistingWhenGetMemberByNameIsInvokedThenMemberIsObtained() {
    // act
    Member target = memberRepository.getMemberByName("Name3");

    // assert
    assertNull(target);
  }

  @Test
  void givenIdentifierOfMemberExistingWhenGetMemberByNameIsInvokedThenMemberIsObtained() {

    // act
    Member target = memberRepository.getMemberByIdentifier(1);

    // assert
    assertNotNull(target);
    assertEquals(defaultMembers.get(0), target);
  }

  @Test
  void givenIdentifierOfMemberNotExistingWhenGetMemberByNameIsInvokedThenMemberIsObtained() {
    // act
    Member target = memberRepository.getMemberByIdentifier(3);

    // assert
    assertNull(target);
  }

  @Test
  void givenParentNodeWhenGetChildMemberIsInvokedThenAllChildrenAreObtained() {

    // act
    Set<Relationship> children = memberRepository.getChildMember(defaultMembers.get(0));

    // assert
    assertNotNull(children);
    assertEquals(2, children.size());
    assertTrue(children.stream().anyMatch(c -> c.getSubordinate().equals(defaultMembers.get(1))));
    assertTrue(children.stream().anyMatch(c -> c.getSubordinate().equals(defaultMembers.get(2))));
  }

  @Test
  void givenLeafNodeWhenGetChildMemberIsInvokedThenAllChildrenAreObtained() {

    // act
    Set<Relationship> children = memberRepository.getChildMember(defaultMembers.get(1));

    // assert
    assertNotNull(children);
    assertEquals(0, children.size());
  }

  @Test
  void givenNotExistingNodeWhenGetChildMemberIsInvokedThenAllChildrenAreObtained() {

    // act
    Assertions.assertThrows(MemberNotFound.class, () -> {
      memberRepository.getChildMember(new Member.Builder().withIdentifier(78).build());
    });


  }

  @Test
  void givenRootNodeWhenGetParentMemberIsInvokedThenIsNotObtained() {

    // act
    Set<Relationship> children = memberRepository.getParentMember(defaultMembers.get(0));

    // assert
    assertNotNull(children);
    assertEquals(0, children.size());

  }

  @Test
  void givenLeafNodeWhenGetParentMemberIsInvokedThenAllChildrenAreObtained() {

    // act
    Set<Relationship> parent = memberRepository.getParentMember(defaultMembers.get(1));

    // assert
    assertNotNull(parent);
    assertEquals(1, parent.size());
    assertTrue(parent.stream().anyMatch(p -> p.getBoss().equals(defaultMembers.get(0))));
  }

  @Test
  void givenNotExistingNodeWhenGetParentMemberIsInvokedThenAllChildrenAreObtained() {

    // act
    Assertions.assertThrows(MemberNotFound.class, () -> {
      memberRepository.getParentMember(new Member.Builder().withIdentifier(78).build());
    });


  }

  @Test
  void givenMemberOnlyInSameDepthWhenGetAllMemberWithSameDepthIsInvokedThenIsNotObtained() {

    // act
    List<Member> members = memberRepository.getAllMemberWithSameDepth(defaultMembers.get(0));

    // assert
    assertNotNull(members);
    assertEquals(0, members.size());

  }

  @Test
  void givenMemberWhenGetAllMemberWithSameDepthThenAllMemberWithSameDepthAreObtained() {

    // act
    List<Member> members = memberRepository.getAllMemberWithSameDepth(defaultMembers.get(1));

    // assert
    assertNotNull(members);
    assertEquals(1, members.size());
    assertTrue(members.contains(defaultMembers.get(2)));
  }

  @Test
  void givenNotExistingNodeWhenGetAllMemberWithSameDepthIsInvokedThenAllChildrenAreObtained() {

    // act
    Assertions.assertThrows(MemberNotFound.class, () -> {
      memberRepository.getAllMemberWithSameDepth(new Member.Builder().withIdentifier(78).build());
    });
  }

  @Test
  void givenGraphWithMemberWhenGetAllMemberThenAllMemberAreObtained() {

    // act
    List<Member> members = memberRepository.getAllMember();

    // assert
    assertNotNull(members);
    assertEquals(3, members.size());
    assertTrue(members.containsAll(defaultMembers));
  }

  @Test
  void givenParentNodeWhenGetNumberOfChildMemberIsInvokedThenNumberIsObtainerObtained() {

    // act
    Integer target = memberRepository.getNumberOfChildMember(defaultMembers.get(0));

    // assert
    assertNotNull(target);
    assertEquals(2, target);
  }

  @Test
  void givenLeafNodeWhenGetNumberOfChildMemberThenNumberIsObtained() {

    // act
    Integer target = memberRepository.getNumberOfChildMember(defaultMembers.get(1));

    // assert
    assertNotNull(target);
    assertEquals(0, target);
  }

  @Test
  void givenNotExistingNodeWhenGetNumberOfChildMemberIsInvokedThenExceptionIsObtained() {

    // act
    Assertions.assertThrows(MemberNotFound.class, () -> {
      memberRepository.getNumberOfChildMember(new Member.Builder().withIdentifier(78).build());
    });
  }

  @Test
  void givenExistingMemberWhenExistsMemberThenTrueObtained() {

    // act
    Boolean target = memberRepository.existsMember(defaultMembers.get(1).getIdentifier());

    // assert
    assertNotNull(target);
    assertTrue(target);
  }

  @Test
  void givenNotExistingMemberWhenExistsMemberIsInvokedThenFalseIsObtained() {

    // act
    Boolean target = memberRepository.existsMember(32);

    // assert
    assertNotNull(target);
    assertFalse(target);
  }

  @Test
  void givenExistingMemberWhenInsertMemberIsInvokedThenExceptionObtained() {

    // act
    Assertions.assertThrows(OperationNotAllowed.class, () -> {
      memberRepository.insertMember(defaultMembers.get(1));
    });

  }

  @Test
  void givenNotExistingMemberWhenInsertMemberIsInvokedThenWorks() {

    // arrange
    Member targetToInsert = new Member.Builder().withIdentifier(32).withName("Jaime").withBirthDate(LocalDateTime.now()).build();

    // act
    memberRepository.insertMember(targetToInsert);

    // assert
    assertTrue(memberRepository.getAllMember().contains(targetToInsert));
  }

  @Test
  void givenExistingMembersWhenInsertRelationShipIsInvokedThenWorks() {

    //arrange
    Member targetToInsert = new Member.Builder().withIdentifier(32).withName("Jaime").withBirthDate(LocalDateTime.now()).build();
    memberRepository.insertMember(targetToInsert);

    // act
    memberRepository.insertRelationShip(defaultMembers.get(2), targetToInsert);

    // assert
    assertTrue(1 == memberRepository.getNumberOfChildMember(defaultMembers.get(2)));
  }

  @Test
  void givenNotExistingMemberWhenInsertRelationShipThenException() {
    //arrange
    Member targetToInsert = new Member.Builder().withIdentifier(32).withName("Jaime").withBirthDate(LocalDateTime.now()).build();

    // act
    Assertions.assertThrows(OperationNotAllowed.class, () -> {
      memberRepository.insertRelationShip(defaultMembers.get(2), targetToInsert);
    });
  }

  @Test
  void givenExistingMembersWhenInsertMemberInLeafNodeIsInvokedThenExceptionIsObtained() {

    // act
    Assertions.assertThrows(OperationNotAllowed.class, () -> {
      memberRepository.insertMemberInLeafNode(defaultMembers.get(1));
    });
  }

  @Test
  void givenNonExistingMembersWhenInsertMemberInLeafNodeIsInvokedThenWorks() {
    //arrange
    Member targetToInsert = new Member.Builder().withIdentifier(32).withName("Jaime").withBirthDate(LocalDateTime.now()).build();

    // act
    memberRepository.insertMemberInLeafNode(targetToInsert);

    // assert
    assertTrue(4 == memberRepository.getAllMember().size());

  }

  @Test
  void givenExistingMembersWhenRemoveMemberIsInvokedThenTrueIsObtained() {

    // act
    Boolean target = memberRepository.removeMember(defaultMembers.get(1));

    // assert
    assertTrue(target);
  }

  @Test
  void givenNonExistingMembersWhenInsertMemberInLeafNodeIsInvokedThenFalseIsObtainer() {
    // act
    Boolean target = memberRepository.removeMember(new Member.Builder().build());

    // assert
    assertFalse(target);

  }

  @Test
  void givenNonExistingMembersWhenRemoveIncomingEdgeIsInvokedThenFalseIsObtained() {
    // act
    Assertions.assertThrows(OperationNotAllowed.class, () -> {
      memberRepository.removeIncomingEdge(new Member.Builder().build());
    });
  }

  @Test
  void givenNonExistingMembersWhenRemoveEdgeIsInvokedThenNullIsObtained() {
    // act
    Relationship target = memberRepository.removeEdge(defaultMembers.get(0),new Member.Builder().build());

    // assert
    assertNull(target);

  }

  @Test
  void givenExistingMembersWhenRemoveEdgeIsInvokedThenRelationShipIsObtained() {
    // act
    Relationship target = memberRepository.removeEdge(defaultMembers.get(0),defaultMembers.get(1));

    // assert
    assertNotNull(target);
    assertEquals(defaultMembers.get(0), target.getBoss());
    assertEquals(defaultMembers.get(1), target.getSubordinate());

  }

  //  @Test
  //  void removeEdge() {
  //  }
}