package com.stratio.test.mafia.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.converter.MemberToMemberResponseConverter;
import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.ReportService;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ReportServiceImplTest {

  private static final Integer DEFAULT_SUBORDINATES_TO_SURVEILLANCE = 50;

  private static ReportService reportService;

  private static MemberRepository repository;

  private static MemberToMemberResponseConverter converter;

  @BeforeAll
  static void beforeAll() {
    repository = mock(MemberRepository.class);
    converter = mock(MemberToMemberResponseConverter.class);
    reportService = new ReportServiceImpl(DEFAULT_SUBORDINATES_TO_SURVEILLANCE,repository, converter);
  }

  @Test
  void whenSpecialSurveillanceIsInvokedThenListIsObtained() {

    // arrange
    Member member1 = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    Member member2 = new Member.Builder().withIdentifier(2).withName("Name2").withBirthDate(LocalDateTime.now()).build();
    MemberResponse memberResponse =
        new MemberResponse.Builder().withIdentifier(1).withName("Name1").withBirthDate(member1.getBirthDate()).build();

    when(repository.getAllMember()).thenReturn(List.of(member1, member2));
    when(repository.getNumberOfChildMember(member1)).thenReturn(52);
    when(repository.getNumberOfChildMember(member2)).thenReturn(12);

    when(converter.convert(member1)).thenReturn(memberResponse);

    // act
    List<MemberResponse> members = reportService.specialSurveillance();

    // assert
    assertNotNull(members);
    assertEquals(1, members.size());
    assertEquals(memberResponse, members.get(0));
  }
}