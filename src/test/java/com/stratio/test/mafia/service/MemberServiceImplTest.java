package com.stratio.test.mafia.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.stratio.test.mafia.controller.vo.MemberResponse;
import com.stratio.test.mafia.converter.MemberToMemberResponseConverter;
import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.MemberService;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class MemberServiceImplTest {

  private static MemberService service;

  private static MemberRepository repository;

  private static MemberToMemberResponseConverter converter;

  @BeforeAll
  static void beforeAll() {
    repository = mock(MemberRepository.class);
    converter = mock(MemberToMemberResponseConverter.class);
    service = new MemberServiceImpl(repository, converter);
  }

  @Test
  void givenExistingMemberWhenGetMemberIsInvokedThenMemberIsObtained() {
    // arrange
    Member member = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    MemberResponse memberResponse =
        new MemberResponse.Builder().withIdentifier(1).withName("Name1").withBirthDate(member.getBirthDate()).build();
    when(repository.getMemberByIdentifier(any())).thenReturn(member);
    when(converter.convert(member)).thenReturn(memberResponse);

    // act
    MemberResponse target = service.getMember(1);

    // assert
    assertNotNull(target);
    assertEquals(memberResponse, target);

  }

  @Test
  void givenNotExistingMemberWhenGetMemberIsInvokedThenMemberIsException() {
    // arrange
    Member member = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    MemberResponse memberResponse =
        new MemberResponse.Builder().withIdentifier(1).withName("Name1").withBirthDate(member.getBirthDate()).build();
    when(repository.getMemberByIdentifier(any())).thenReturn(null);

    // act
    Assertions.assertThrows(MemberNotFound.class, () -> {
          MemberResponse target = service.getMember(1);
        });


  }
}