package com.stratio.test.mafia.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.stratio.test.mafia.domain.Member;
import com.stratio.test.mafia.domain.Relationship;
import com.stratio.test.mafia.exception.OperationNotAllowed;
import com.stratio.test.mafia.repository.MemberRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class PrisonServiceImplTest {

  private static PrisonServiceImpl prisonService;

  private static MemberRepository repository;

  private static Member memberInPrison;

  private static Member lastBoss;

  private static List<Member> lastSubordinates;

  @BeforeAll
  static void beforeAll() {
    lastBoss = new Member.Builder().withIdentifier(6).withName("Name6").withBirthDate(LocalDateTime.now()).build();
    lastSubordinates = List.of(new Member.Builder().withIdentifier(7).withName("Name7").withBirthDate(LocalDateTime.now()).build());
    HashMap<Integer, Member> map = new HashMap<>();
    memberInPrison = new Member.Builder().withIdentifier(5).withName("Name5").withBirthDate(LocalDateTime.now()).build();
    memberInPrison.setLastBoss(lastBoss);
    memberInPrison.setLastSubordinates(lastSubordinates);
    map.put(5, memberInPrison);
    repository = mock(MemberRepository.class);
    prisonService = new PrisonServiceImpl(repository, map);


  }

  @Test
  public void givenMemberRootInPrisonWhenGoToJailIsInvokedThenExceptionIsObtained() {

    // arrange
    Member member = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    Member member2 = new Member.Builder().withIdentifier(2).withName("Name2").withBirthDate(LocalDateTime.now()).build();
    Relationship relationship = mock(Relationship.class);
    when(relationship.getBoss()).thenReturn(member);
    when(relationship.getSubordinate()).thenReturn(member2);

    when(repository.getMemberByIdentifier(1)).thenReturn(member);
    when(repository.getChildMember(member)).thenReturn(Set.of(relationship));
    when(repository.getParentMember(member)).thenReturn(new HashSet<>());
    when(repository.getAllMemberWithSameDepth(member)).thenReturn(new ArrayList<>());

    // act
    prisonService.goToJail(1);

    // assert
    verify(repository, times(3)).insertRelationShip(any(), any());
    verify(repository).removeMember(member);

  }

  @Test
  public void givenMemberLeafInPrisonWhenGoToJailIsInvokedThenExceptionIsObtained() {

    // arrange
    Member member = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    Member member2 = new Member.Builder().withIdentifier(2).withName("Name2").withBirthDate(LocalDateTime.now()).build();
    Relationship relationship = mock(Relationship.class);
    when(relationship.getBoss()).thenReturn(member);
    when(relationship.getSubordinate()).thenReturn(member2);

    when(repository.getMemberByIdentifier(2)).thenReturn(member2);
    when(repository.getChildMember(member)).thenReturn(new HashSet<>());
    when(repository.getParentMember(member)).thenReturn(Set.of(relationship));

    // act
    prisonService.goToJail(2);

    // assert
    verify(repository, times(2)).getAllMemberWithSameDepth(any());
    verify(repository, times(3)).insertRelationShip(any(), any());
    verify(repository).removeMember(member2);

  }

  @Test
  public void givenMemberIntermediateInPrisonWhenGoToJailIsInvokedThenExceptionIsObtained() {

    // arrange
    Member member = new Member.Builder().withIdentifier(1).withName("Name1").withBirthDate(LocalDateTime.now()).build();
    Member member2 = new Member.Builder().withIdentifier(2).withName("Name2").withBirthDate(LocalDateTime.now()).build();
    Member member3 = new Member.Builder().withIdentifier(3).withName("Name3").withBirthDate(LocalDateTime.now()).build();
    Relationship relationship = mock(Relationship.class);
    when(relationship.getBoss()).thenReturn(member);
    when(relationship.getSubordinate()).thenReturn(member2);

    Relationship relationship2 = mock(Relationship.class);
    when(relationship2.getBoss()).thenReturn(member2);
    when(relationship2.getSubordinate()).thenReturn(member3);

    when(repository.getMemberByIdentifier(2)).thenReturn(member2);
    when(repository.getChildMember(member2)).thenReturn(Set.of(relationship2));
    when(repository.getParentMember(member2)).thenReturn(Set.of(relationship));
    when(repository.getAllMemberWithSameDepth(any())).thenReturn(new ArrayList<>());

    // act
    prisonService.goToJail(2);

    // assert
    verify(repository, times(1)).getAllMemberWithSameDepth(any());
    verify(repository, times(3)).insertRelationShip(any(), any());
    verify(repository).removeMember(member2);

  }

  @Test
  public void givenMemberInPrisonWhenReleasedFromPrisonIsInvokedThenOK() {

    // arrange
    when(repository.existsMember(5)).thenReturn(Boolean.FALSE);
    when(repository.existsMember(6)).thenReturn(Boolean.TRUE);
    when(repository.existsMember(7)).thenReturn(Boolean.TRUE);
    when(repository.getMemberByIdentifier(7)).thenReturn(lastSubordinates.get(0));
    when(repository.getMemberByIdentifier(6)).thenReturn(lastBoss);

    // act
    prisonService.releasedFromPrison(5);

    // assert
    verify(repository).removeIncomingEdge(any());
    verify(repository).insertMember(any());
    verify(repository, times(2)).insertRelationShip(any(), any());
  }

  @Test
  public void givenMemberReleasedWhenReleasedFromPrisonIsInvokedThenOExceptionIsObtained() {
    // arrange
    when(repository.existsMember(6)).thenReturn(Boolean.TRUE);

    // act
    Assertions.assertThrows(OperationNotAllowed.class, () -> {
      prisonService.releasedFromPrison(6);
    });

  }
}