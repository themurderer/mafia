package com.stratio.test.mafia.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.stratio.test.mafia.exception.MemberNotFound;
import com.stratio.test.mafia.repository.MemberJgraphtRepository;
import com.stratio.test.mafia.repository.MemberRepository;
import com.stratio.test.mafia.service.api.HireService;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class HireServiceImplTest {

  private static HireService hireService;

  private static MemberRepository repository;

  @BeforeAll
  static void beforeAll() {
    repository = mock(MemberJgraphtRepository.class);
    hireService = new HireServiceImpl(repository);
  }

  @Test
  void givenNotExistingMemberWhenHireNewMemberIsInvokedThenExceptionIsObtained() {
    // act
    hireService.hireNewMember(1, "Pepe", LocalDateTime.now(), null);

    // assert
    verify(repository).insertMemberInLeafNode(any());

  }

  @Test
  void givenNotExistingMemberWithBotNoExistingWhenHireNewMemberIsInvokedThenExceptionIsObtained() {
    // arrange
    when(repository.getMemberByIdentifier(1)).thenReturn(null);

    // act
    Assertions.assertThrows(MemberNotFound.class, () -> {
      hireService.hireNewMember(1, "Pepe", LocalDateTime.now(), 1);
    });

  }
}